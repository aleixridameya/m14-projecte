### Deploy

Realitzar un desplagament amb docker stack, mentres pertanys a un swarm amb 2 pc i comprobar que hi ha mes d'un host en el comptador, a mes a mes utilitzar docker service per mostrar els serveis i canviar el numero de repliques de web.

### 1 Iniciar el swarm
```
docker swarm init

Swarm initialized: current node (z6bdy9z4mrje9wgrh7cd0feze) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4hmzybctmayzkklhj23hhrx42yyvajzef23bl4fsnbjcp79grz-91f1h1n0v42l4jwga8brscaxd 10.200.243.209:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

### 2 Compartir el tocken de worker i a un altre host realitzar:
```
docker swarm join --token SWMTKN-1-4hmzybctmayzkklhj23hhrx42yyvajzef23bl4fsnbjcp79grz-91f1h1n0v42l4jwga8brscaxd 10.200.243.209:2377
```
#### Comprovació 
```
docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
oghp73g1f1f9ajffdmlot98l5     i07        Ready     Active                          24.0.2
ufrv9d800is3f8qfv4l31gtii *   i09        Ready     Active         Leader           24.0.2
webkm9eys0j8hlrvj0tcs5xye     i08        Ready     Active                          24.0.6

```

### 3 Desplger el compose.yml utilitzant docker stack
#### El nom del stack es dirà myapp
```
docker stack deploy -c compose.yml myapp
Creating network myapp_webnet
Creating service myapp_web
Creating service myapp_redis
```

#### Comprovar que hi ha mes d'un host desde el visualizer
```
En el navegador:
localhost:80 

Sortira la web i al fer f5 el hostname anira canviant.
```

### 4 Canviar el numero de replicas en calent

#### Replicas abans del canvi
```
docker stack services myapp
ID             NAME          MODE         REPLICAS   IMAGE                             PORTS
ecprmrpsxsj4   myapp_redis   replicated   1/1        redis:latest                      *:6379->6379/tcp
wz3b56uzn7me   myapp_web     replicated   2/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```
#### Editem el fitxer compose.yml per canviar el numero de replicas. Apliquem els canvis
```
docker stack deploy -c compose.yml myapp
Updating service myapp_web (id: wz3b56uzn7meufe2eamrqb34i)
Updating service myapp_redis (id: ecprmrpsxsj4n40x6smm8jvio)
```

#### Comprovació
```
docker stack services  myapp
ID             NAME          MODE         REPLICAS   IMAGE                             PORTS
ecprmrpsxsj4   myapp_redis   replicated   1/1        redis:latest                      *:6379->6379/tcp
wz3b56uzn7me   myapp_web     replicated   5/5        edtasixm05/getstarted:comptador   *:80->80/tcp
```
