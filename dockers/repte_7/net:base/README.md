## Servidor net

### 1 Dockerfile
```
# Servidor net que implementa serveis de xarxa: echo,daytime,chargen,ftp,tftp i http

FROM debian:latest
LABEL authot="Aleix Ridameya"
LABEL subject="serveidor net"
RUN apt-get update
RUN apt-get install -y xinetd iproute2 iputils-ping nmap procps net-tools apache2 tftp vsftpd wget telnet vim ftp
COPY daytime-stream chargen-stream echo-stream /etc/xinetd.d/
COPY index.html /var/www/html/index.html
COPY vsftpd.conf /etc/
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
RUN chmod +x /opt/docker/useradd.sh
RUN bash /opt/docker/useradd.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 7 13 19 80 69 21 20
```
```
Ports ftp: 20 21
      tftp: 69
      http: 80
      echo: 7
      chargen: 19
      daytime: 13

Serveis xinted: servidor net (chargen, daytime, echo)
        apache2: servei http
        tftp: tftp
        vsftpd: ftp
```

### 2 Startup.sh
```
#! /bin/bash

service vsftpd start 
apachectl start 
/usr/sbin/xinetd -dontfork
```

### 3 Fitxer vsftp.conf ( ftp )
```
És el fitxer de configuració del ftp, afagirem "local_root=/srv/ftp" per a que la carpeta dels usuaris sigui compartida.
Descomentar lineas
local_enable=YES
write_enable=YES
local_umask=YES
ftpd_banner=Welcome to blah FTP service.
```

### 4 useradd.sh
```
Per a poder utlitzar el ftp creant carpetas.... Haurem de crear usuaris locals en el servidor. Per aixo farem un script que crea usuaris.

#! /bin/bash

# Creació dels usuaris per verificat accés al ftp
for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
done
```

### 3 Crear imatge
```
docker build -t aleixridameya/deb_r7_net:base .
```
### 4 Iniciar docker
```
docker run --rm --name net -h net.edt -p 80:80 -d aleixridameya/deb_r7_net:base
``` 
### 5 Compravacions echo, chargen, daytime, http
```
Desde google on es posa la url posar localhost o IP del docker.
I inicialitzar un altre cop el servidor pero aquest s'utilitzara com a client:
  - docker run --rm --name client -h net.edt -p 80:80 -d aleixridameya/deb_r7_net:base
    Dintre d'aquest docker 
    telnet (ip_container) (port)
    telnet 172.17.0.2 13
    telnet 172.17.0.2 19
    telnet 172.17.0.2 7 

    En el cas del 7 i 19 per sortir hem de clicar Ctrl+Alt Gr+] i despres posar quit

```
#### -Comprovació fitxer ftp
```
ftp (ip)
ftp 172.17.0.2
En 
```
