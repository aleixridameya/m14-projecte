## Repte 10: Repliques de docker-compose

### docker-compose.yml
```
Per utilitzar els ports dinàmics on posem els ports és important només posar "80". (El port canviara segons el servei que volguem)"


version: "3"
services:
  web:
    ##image: aleixridameya/getstarted:comptador
    image: edtasixm05/getstarted:comptador
    ports:
      - "80"
    networks:
      - webnet
  web2:  
    image: aleixridameya/web22:detach
    ports:
      - "80"
    networks:
      - webnet
....
```

### Comprovacio 
```
docker-compose up -d

docker ps
CONTAINER ID   IMAGE                             COMMAND                  CREATED         STATUS         PORTS                                                           NAMES
2132b040245d   aleixridameya/web22:detach        "/bin/sh -c /opt/doc…"   4 minutes ago   Up 4 minutes   0.0.0.0:32771->80/tcp, :::32771->80/tcp                         repte10-web2-1
8b5bc1d618b0   edtasixm05/getstarted:comptador   "python app.py"          4 minutes ago   Up 4 minutes   0.0.0.0:32770->80/tcp, :::32770->80/tcp                         repte10-web-1


Podem veure que les dos webs estan fent servir el port 80 de forma dinàmica.
Per veure la web: localhost: localhost:32770, localhost:32771
```

###  docker compose up -d --scale web=2
```
Aquesta ordre ens permet desplegar n rèpliques del container escollit. No poden tenir un nom fixe ni un port fixer.
```

