## Docker compose web+portainer

### Fitxer docker-compose-yml

```
version: "2"
services:
  web:
    image: aleixridameya/deb_r2
    container_name: web
    hostname: web
    ports:
      - "80:80"
    networks:
      - mynet
  portainer:
    image: portainer/portainer
    container_name: portainer
    hostname: portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - mynet
networks:
  mynet: 

```

### Inicar compose
```
docker compose up -d
```

### Comprobacions:

```
docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED              STATUS              PORTS                                                           NAMES
28b9c3df50b4   portainer/portainer    "/portainer"             About a minute ago   Up About a minute   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   portainer
75aa977b6f53   aleixridameya/deb_r2   "/bin/sh -c /opt/doc…"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp, :::80->80/tcp                               web

- Per provar la web anar al google i posar localhost:80

- Per provar el portainer anar al google i posar localhost:9000
```
