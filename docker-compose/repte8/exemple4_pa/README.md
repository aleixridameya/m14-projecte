## Docker compose servidor postgres + adminer

### Fitxer docker-compose.yml

```
version: "2"
services:
  db:
    image: aleixridameya/deb_r3:base
    restart: always
    container_name: postgres
    hostname: postgres
    environment: 
      POSTGRES_PASSWORD: passwd
      POSTGRES_DB: training
    networks:
      - mynet
  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
    networks:
      - mynet
networks:
  mynet:

```

### Iniciar compose
```
docker compose up -d
```

### Comprovacions
```
docker ps
CONTAINER ID   IMAGE                       COMMAND                  CREATED          STATUS          PORTS                                       NAMES
fa157bc9e757   aleixridameya/deb_r3:base   "docker-entrypoint.s…"   23 seconds ago   Up 20 seconds   5432/tcp                                    postgres
a5ad59f6b389   adminer                     "entrypoint.sh php -…"   23 seconds ago   Up 20 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   exemple4_pa-adminer-1

Anar al navegador i posar localhost:8080

Contrasenyas establertes en el docker compose...
Entrar amb adminer i iniciar sessió conectante amb la database training.
```

