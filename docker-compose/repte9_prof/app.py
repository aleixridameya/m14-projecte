import time

import redis #redis: bases de dades clau valor (en el nostre cas utilitzem en pytho)
from flask import Flask #flask: una eina per desenvolupar aplicacions web de manera rapida

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379) #cache= (variable per contactar amb el redis)

def get_hit_count(): # Funcio que compta les vegades que entres a la pagina
    retries = 5
    while True:
        try: # Prova de conectar amb el redis i retorna un valor
            return cache.incr('hits') # Tornam el valor de hits + 1
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)


@app.route('/patata')
def myhola():
    count = get_hit_count()
    return 'Per payaya el lwandosky'

