## Creació d'un servidor ldap en mode detach

### 1 Crear Dockerfile
```
#ldap server

FROM debian:latest
LABEL author="aleix"
LABEL subjecte="ldapserver"
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
ARG DEBIAN_FRONTED=noninteractive
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT ["/opt/docker/startup.sh"]
EXPOSE 389
```

### 2 Crear startup.sh
```
#! /bin/bash

function initdb() {
# Server bàsic ldap, amb base de dades edt.org. Aquesta imatge engega amb CMD un script anomenat startup.sh que fa el següent:
# 1 Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# 2  Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.confi
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3  Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4  Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 5  Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
}

function startdb() {
/usr/sbin/slapd -d0
}

case $1 in
	initdb)
	echo "Inicialitzant la base de dades"
	initdb;;
	startdb)
	echo "Engengant la base de dades"
	startdb;;
esac

```

### 3 Crear slapd.conf i edt-org.ldif
```
Copiem un fitxer predefinit
```

### 4 Crear imatge
```
docker build -t aleixridameya/deb_r4:base .
```

### 5 Creem els volums
```
docker volume create slapd-conf
docker volume create slapd-d
```

### 6 Iniciem container
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -d aleixridameya/deb_r4:base initdb
```
### 7 Probem si funciona la persistencia de dades
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' UID=pau
ldapdelete -vx -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org' 
docker stop ldap.edt.org
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -d aleixridameya/deb_r4:base startdb
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' UID=pau # si no surt res esta bé.
```

