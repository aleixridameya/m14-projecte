#! /bin/bash

# Creació de la xixa 

#Public
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#Privat
mkdir /var/lib/samba/privat
# chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.

#Establir la conf de samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

#Creació dels usuaris samba/unix
useradd -m -s /bin/bash aleix
echo -e "aleix\naleix" | smbpasswd -a aleix

#Activar els serveis
/usr/sbin/smbd 
/usr/sbin/nmbd -F 
