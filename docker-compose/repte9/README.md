## Comptador de visites

### Fitxers necesaris:
```
app.py  docker-compose.yml  Dockerfile  requirements.txt
```

### app.py
```
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```

### Dockerfile

```
# syntax=docker/dockerfile:1
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
```

### requirements.txt
```
flask
redis
```

### docker-compose.yml
```
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development

  redis:
    image: "redis:alpine"
```

### Comprovacions

```
Docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                                       NAMES
5f1f4e5c97a7   repte9-web     "flask run"              5 minutes ago   Up 5 minutes   0.0.0.0:5000->5000/tcp, :::5000->5000/tcp   repte9-web-1
a91364f21767   redis:alpine   "docker-entrypoint.s…"   5 minutes ago   Up 5 minutes   6379/tcp                                    repte9-redis-1


Anar al navegador i posar localhost:5000 sino funciona posar 127.0.0.1:5000
```
