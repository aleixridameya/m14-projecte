## Repte 3 Utilització de volums en un servidor Postgres

### 1- Creació del Dockerfle
```
FROM postgres:latest
LABEL author="aleix"
LABEL subjecte="postgres_server"
COPY trainingv7.sql /docker-entrypoint-initdb.d/
```

### 2.- Creació imatge
```
docker build -t aleixridameya/deb_r3:base .
```

### 3- creació del volum
```
docker volume create postgres_v 
```

### 4- Iniciar el docker
```
docker run --rm --name repte3 -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres_v:/var/lib/postgresql/data -d aleixridameya/deb_r3:base
```

### 5- Accedir al docker
```
docker run -it --rm postgres psql -h 172.17.0.2 -U postgres -d training
```

### 6- Compravar la persistència de dades ( despres de accedir al docker)
```
DROP TABLE clients CASCADE;
(sortir del docker)
docker stop repte3
docker run --rm --name repte3 -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres_v:/var/lib/postgresql/data -d aleixridameya/deb_r3:base
docker run -it --rm postgres psql -h 172.17.0.2 -U postgres -d training
\d (veiem que la taula esborrada ja no esta)
```

