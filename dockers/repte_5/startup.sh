#! /bin/bash

#! /bin/bash

function initdb() {
# Server bàsic ldap, amb base de dades edt.org. Aquesta imatge engega amb CMD un script anomenat startup.sh que fa el següent:
# 1 Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# 2  Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.confi
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3  Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4  Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 5  Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
}

function startdb() {
/usr/sbin/slapd -d0
}

function slapd() {
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0

}

case $1 in
	initdb)
	initdb;;
	"start"|"edtorg")
	startdb;;
	"slapd")
	slapd;;
	"slapcat")
	if [ $2 -eq 0 -o $2 -eq 1 ];then
	  slapcat -n$2
	else
	  slapcat
	fi;;
        *)
	startdb;;
esac

