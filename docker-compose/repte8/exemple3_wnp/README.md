## Docker compose servidor net + portainer + web

### Fitxer docker-compose.yml

```
version: "2"
services:
  web:
    image: aleixridameya/deb_r2
    container_name: web
    hostname: web
    ports:
      - "80:80"
    networks:
      - mynet
  net:
    image: aleixridameya/net:base
    container_name: net
    hostname: net
    ports:
      - "7:7"
      - "13:13"
      - "19:19"
    networks:
      - mynet
  portainer:
    image: portainer/portainer
    container_name: portainer
    hostname: portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - mynet
networks:
  mynet:
```

### Iniciar compose
```
docker compose up -d
```

### Comprovacions
```
docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED              STATUS         PORTS                                                                                                                            NAMES
2d1aaa17fca1   portainer/portainer      "/portainer"             6 seconds ago        Up 5 seconds   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp                                                                    portainer
f83543523979   aleixridameya/net:base   "/bin/sh -c /opt/doc…"   6 seconds ago        Up 5 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 20-21/tcp, 69/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp, 80/tcp   net
25534c80c24b   aleixridameya/deb_r2     "/bin/sh -c /opt/doc…"   About a minute ago   Up 5 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp                                                                                                web

Per comprovar la web anar al navegador i posar localhost:80
Per comprovar el portainer anar al navegador i posar localhost:9000

Per comprovar el servidor net:
telnet 172.20.0.3 7 (echo)
telnet 172.20.0.3 13 (date)
telnet 172.20.0.3 19 (chargen)
```

