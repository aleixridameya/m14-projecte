## Get started with swarm

Un swarm és un conjunt de hosts que treballa com a un sol.

### 1 Creació del swarm
```
docker swarm init
Swarm initialized: current node (qinon224j0zbymnv0izab5pfp) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-30yjs0ko31lda72lveu3oy8ev7pkqp0di4t9a9yzmol0jxvlb9-evmfv0lherpvhve7r3b63j9ao 10.200.243.209:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

```

### 2 Compartir el tocken de worker i a un altre host realitzar:
```
docker swarm join --token SWMTKN-1-30yjs0ko31lda72lveu3oy8ev7pkqp0di4t9a9yzmol0jxvlb9-evmfv0lherpvhve7r3b63j9ao 10.200.243.209:2377
```

### 3 Desplegar el servei
```
 docker service create --replicas 1 --name helloworld alpine ping docker.com
sqjpmjva77li3xgoi6fhbq4z9
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
verify: Service converged 
```

### Inspeccionar el sevei

```
docker service inspect --pretty helloworld : es veu de forma mes compacte 
docker service inspect helloworld
```

### Realitzar scale
```
docker service scale helloworld=5
helloworld scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged 

Compravació:
docker service ps helloworld
```
### Esborrar servei
```
docker service rm helloworld
helloworld
```
