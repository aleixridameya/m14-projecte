# Pujar al Vagrant Cloud

### Iniciar sessió

Per iniciar sessió hem de tenir un compte de Vagrant Cloud. Una vegada tenim el compte anem a settings --> security i generem un tocken. Desde la terminal ens autenticarem.
```
vagrant login --token TU_TOKEN
```

### Publiquem 
```
vagrant cloud publish aleixridameya/edt22base 1.0.0 virtualbox Vagrantfile --force --release
You are about to publish a box on Vagrant Cloud with the following options:
aleixridameya/edt22base:   (v1.0.0) for provider 'virtualbox'
Automatic Release:     true
Box Architecture:      amd64
Saving box information...
Uploading provider with file /home/users/inf/hisx2/a211345ar/Documents/m14-projecte/vagrant/repte3/Vagrantfile
Releasing box...
Complete! Published aleixridameya/edt22base
Box:                  aleixridameya/edt22base
Private:              yes
Version:              1.0.0
Provider:             virtualbox
Architecture:         amd64
Default Architecture: yes
```

### Ordre principal
```
vagrant cloud publish aleixridameya/edt22base 1.0.0 virtualbox Vagrantfile --force --release
```
