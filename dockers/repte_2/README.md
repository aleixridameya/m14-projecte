## Repte_02

```
Servidor detach amb web
(Servidor detach amb propagació de port)
```
### 1.  Crear carpeta de context
```
mkdir repte_2
cd repte_2
```

### 2- Crear el Dockerfile:
```
# repte 02
# Servidor detach amb propagació de port

FROM debian:latest
LABEL author="aleix ridameya"
LABEL subject="webserver"
RUN apt-get update
RUN apt-get -y install procps iproute2 nmap vim tree apache2
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker/
CMD /opt/docker/startup.sh
EXPOSE 80
```
### 3- Crear startup.sh
```
#! /bin/bash
cp /opt/docker/index.html /var/www/html/index.html

# mantenir apache en primer pla
apachectl -k start -X

```
### 4- Crear index.html
```
<html>
	<head>
		<title>web repte_02 Dockers</title>
	</head>
	<body>
		<h1>web 2hisix</h1>
		<h2>repte_02 docker</h2>
		hola soc la pagina web d'exemple
	</body>
</html>
```
### 5- Crear imatge
```
docker build -t aleixridameya/deb_r2 .
```
### 6- Provar imatge
```
# -p 80:80 propagació de port
docker run --rm -h r2 -p 80:80 -d aleixridameya/deb_r2 
```
