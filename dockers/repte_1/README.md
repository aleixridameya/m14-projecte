## Repte_01

```
Imatge que permeti fer ps, ping, ip, nmap, tree, vim.
```
### 1.  Crear carpeta de context
```
mkdir repte1
cd repte1
```

### 2- Crear el Dockerfile:
```
# Repte_01 
# servidor amb ps, ping, ip, nmap, tree, vim
FROM debian:latest
LABEL author="aleix ridameya"
LABEL subject="webserver"
RUN apt-get update
RUN apt-get -y install procps iproute2 nmap vim tree iputils-ping
```
### 3- Crear imatge
```
docker build -t aleixridameya/deb_21:latest .
```
### 4- Provar imatge
```
docker run --rm --name repte1 -h r1 -it aleixridameya/deb_r1:latest
```
