## Servidor ssh

### Dockerfile
```
FROM debian
LABEL author="@aleixridameya"
LABEL subject="SSH server"
RUN apt-get update
RUN apt-get install -y openssh-client openssh-server procps vim
COPY useradd.sh /tmp
RUN bash /tmp/useradd.sh
WORKDIR /tmp
CMD /etc/init.d/ssh start -D
EXPOSE 22

```

### useradd.sh

```
#! /bin/bash

# Creació dels usuaris per verificat accés SSH
for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
done
```

### Crear imatge i iniciar-lo amb docker

```
docker build -t aleixridameya/deb_r7_ssh:base .

docker run --rm --name ssh -h ssh -d aleixridameya/deb_r7_ssh:base
```


### Comprovacions

```
docker run --rm --name client -h ssh  -it aleixridameya/deb_r7_ssh:base /bin/bash

ssh unix1@172.17.0.2
unix1@172.17.0.2's password: 
```
