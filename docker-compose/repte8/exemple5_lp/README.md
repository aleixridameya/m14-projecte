## Docker compose servidor ldap + phpldapadmin

### Fitxer docker-compose.yml

```
version: "3"
services:
  ldap:
    image: aleixridameya/ldap23:editat
    container_name: ldapserver
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: aleixridameya/phpldapadmin:base
    container_name: phpldap
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:

```

### Iniciar compose
```
docker compose up -d
```

### Comprovacions
```
docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED         STATUS         PORTS                                   NAMES
96d330d22277   aleixridameya/deb_r6:base    "/bin/sh -c /opt/doc…"   6 minutes ago   Up 6 minutes   0.0.0.0:389->389/tcp, :::389->389/tcp   ldapserver
62711202c6f5   edtasixm06/phpldapadmin:19   "/bin/sh -c /opt/doc…"   6 minutes ago   Up 6 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp       phpldap


Anar al navegador i posar http://localhost/phpldapadmin/

Contra es la definida al fitxer de configuració
```

