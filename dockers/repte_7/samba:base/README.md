## Servidor samba

### Fitxers necessaris
```
Dockerfile
smb.conf
startup.sh
```

### Dockerfile
```
FROM debian:latest
LABEL author="@aleixridameya"
LABEL subject="SAMBA server"
RUN apt-get update
RUN apt-get install -y vim procps samba samba-client
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

### Startup.sh
```
#! /bin/bash

# Creació de la xixa 

#Public
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#Privat
mkdir /var/lib/samba/privat
# chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.

#Establir la conf de samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

#Creació dels usuaris samba/unix
useradd -m -s /bin/bash aleix
echo -e "aleix\naleix" | smbpasswd -a aleix

#Activar els serveis
/usr/sbin/smbd 
/usr/sbin/nmbd -F 
```

### smb.conf
```
[public]
comment = Share de contingut public
path = /var/lib/samba/public
public = yes
browseable = yes
writable = yes
printable = no
guest ok = yes
[privat]
comment = Share d'accés privat
path = /var/lib/samba/privat
public = no
browseable = no
writable = yes
printable = no
guest ok = yes
```

### Crear imatge i inicialitzar container
```
docker build -t aleixridameya/deb_r7_samba:base .

docker run --rm --name samba -h samba_ -p 139:139 -p 445:445 -d aleixridameya/deb_r7_samba:base
```

### Comprabació amb localhost i des de fora
```
docker exec -it samba /bin/bash


localhost:
smbclient //localhost/public -U aleix
Password for [WORKGROUP\aleix]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Mon Oct 23 11:20:14 2023
  ..                                  D        0  Mon Oct 23 11:20:15 2023
  smb.conf                            N      292  Mon Oct 23 11:20:14 2023
  startup.sh                          N      500  Mon Oct 23 11:20:14 2023
  Dockerfile                          N      275  Mon Oct 23 11:20:14 2023
  README.md                           N      106  Mon Oct 23 11:20:14 2023

		61611820 blocks of size 1024. 23265124 blocks available


Des de fora:
smbclient //172.17.0.2/public -U aleix
Password for [WORKGROUP\aleix]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Mon Oct 23 11:20:14 2023
  ..                                  D        0  Mon Oct 23 11:20:15 2023
  smb.conf                            N      292  Mon Oct 23 11:20:14 2023
  startup.sh                          N      500  Mon Oct 23 11:20:14 2023
  Dockerfile                          N      275  Mon Oct 23 11:20:14 2023
  README.md                           N      106  Mon Oct 23 11:20:14 2023

		61611820 blocks of size 1024. 23265124 blocks available
```


