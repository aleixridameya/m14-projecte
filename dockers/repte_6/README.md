## Servidor ldap amb canvi d'admin i passwd

### Dockerfile
```
FROM debian:latest
LABEL author="aleix"
LABEL subjecte="ldapserver"
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
ARG DEBIAN_FRONTED=noninteractive
ENV ldap_admin='Manager'
ENV ldap_admin_passwd='secret'
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

### Startup.sh

```
#! /bin/bash

# Server bàsic ldap, amb base de dades edt.org. Aquesta imatge engega amb CMD un script anomenat startup.sh que fa el següent:
# 1 Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# 2  Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.confi
sed -i 's/Manager/'$ldap_admin'/' /opt/docker/slapd.conf
sed -i 's/secret/'$ldap_admin_passwd'/' /opt/docker/slapd.conf 
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3  Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4  Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 5  Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0

```

## 3 Crear fitxer de configuracio slapd.conf i Crear ditxer de les dades de la base de dades edt-org.ldif
```
Copia un fitxer predefinit
```

## 4 Crear imatge
```
docker build -t aleixridameya/deb_r6:base .
```

## 5 Comprovar el funcionament 
```
docker run --rm --name ldap -e ldap_admin=Pere -e ldap_admin_passwd=contra -d aleixridameya/deb_r6:base
docker exec -it ldap /bin/bash # cat slapd.conf 
docker run --rm --name ldap -d aleixridameya/deb_r6:base
docker exec -it ldap /bin/bash # cat slapd.conf
```
