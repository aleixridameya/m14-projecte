## Servidor ldap amb entrypoint 

### 1 Dockerfile
```
FROM debian:latest
LABEL author="aleix"
LABEL subjecte="ldap server"
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
ARG DEBIAN_FRONTED=noninteractive
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT ["/opt/docker/startup.sh"]
EXPOSE 389
```

### 2 startup.sh
```
#! /bin/bash

function initdb() {
# 1 Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# 2  Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.confi
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3  Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4  Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# 5  Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
}

function startdb() {
/usr/sbin/slapd -d0
}

# Inicialitzar el servei de zero però sense introduir les dades.
function slapd() {
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0

}

case $1 in
	initdb)
	initdb;;
	"start"|"edtorg")
	startdb;;
	"slapd")
	slapd;;
	"slapcat")
	if [ $2 -eq 0 -o $2 -eq 1 ];then
	  slapcat -n$2
	else
	  slapcat
	fi;;
        *)
	startdb;;
esac

```

### 3 Crear fitxer de configuracio slapd.conf i Crear ditxer de les dades de la base de dades edt-org.ldif
```
Copiem un fitxer predefinit
```

### 4 Crear imatge 
```
docker build -t aleixridameya/deb_r5:base .
```

### 5 Crear volumen
```
docker volume create slapd-conf
docker volume create slapd-d
```

### 6 Prova initdb

```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -d aleixridameya/deb_r5:base initdb
ldapdelete -vx -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org' 
docker stop ldap.edt.org
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -d aleixridameya/deb_r5:base initdb 
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' UID=pau #Si el contingut esborrat anteriorament surt esta be
```

### 7 Prova slapd
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -d aleixridameya/deb_r5:base slapd
docker exec -it ldap.edt.org /bin/bash
slapcat -n0
funciona si engega el servidor i no te les dades.
```

### 8 prova start / edtorg / res 
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -d aleixridameya/deb_r5:base edtorg
ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' UID=pau # si no surt res esta bé.
```

### 9 prova slapcat
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -it aleixridameya/deb_r5:base slapcat 1
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -it aleixridameya/deb_r5:base slapcat 0
docker run --rm --name ldap.edt.org -h ldap.edt.org -v slapd-conf:/etc/ldap/slapd.d -v slapd-d:/var/lib/ldap -it aleixridameya/deb_r5:base slapcat 
```


