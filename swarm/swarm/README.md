### Creació del swarm
```
docker swarm init

docker swarm init
Swarm initialized: current node (qinon224j0zbymnv0izab5pfp) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-30yjs0ko31lda72lveu3oy8ev7pkqp0di4t9a9yzmol0jxvlb9-evmfv0lherpvhve7r3b63j9ao 10.200.243.209:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

```

### 2 Compartir el tocken de worker i a un altre host realitzar:
```
docker swarm join --token SWMTKN-1-30yjs0ko31lda72lveu3oy8ev7pkqp0di4t9a9yzmol0jxvlb9-evmfv0lherpvhve7r3b63j9ao 10.200.243.209:2377
```

### Docker node
Estat del node
```
docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
w1wsuqchoh6gxfsgt9648r4wv     i07        Ready     Active                          24.0.2
qinon224j0zbymnv0izab5pfp *   i09        Ready     Active         Leader           24.0.6

docker node update --availability pause i07  ## Es pause el node i no deixa que mes containers s'afageixin
docker node update --availability drain i07  ## S'apaga el/s container/s al host indicat i s'engenant a un altre host.
docker service ps myapp_web
ID             NAME              IMAGE                             NODE      DESIRED STATE   CURRENT STATE                 ERROR     PORTS
ll4o8r5u2t5a   myapp_web.1       edtasixm05/getstarted:comptador   i09       Running         Running 4 minutes ago                   
09efufndyl2m   myapp_web.2       edtasixm05/getstarted:comptador   i09       Running         Running about a minute ago              
qbror44wpll5    \_ myapp_web.2   edtasixm05/getstarted:comptador   i07       Shutdown        Shutdown about a minute ago 
 
docker node update --availability active i07 ## S'activa el node
```

